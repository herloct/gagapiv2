<?php

namespace Kuartet\GagApi\Domains;

final class Gags implements \Iterator, \Countable
{
    private $attributes;

    final public function getAttributes()
    {
        return $this->attributes;
    }

    private $images;

    final public function getImages()
    {
        return $this->images;
    }

    public function __construct(
        Attributes $attributes,
        array $images
    ) {
        $this->attributes = $attributes;
        $this->images = $images;
    }

    // Countable

    public function count()
    {
        return count((array) $this->images);
    }

    // Iterator

    private $position = 0;

    public function current()
    {
        return $this->images[$this->position];
    }

    public function key()
    {
        return $this->position;
    }

    public function next()
    {
        ++$this->position;
    }

    public function rewind()
    {
        $this->position = 0;
    }

    public function valid()
    {
        return isset($this->images[$this->position]);
    }
}
