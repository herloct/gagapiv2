<?php

namespace Kuartet\GagApi\Domains;

final class Gag
{
    private $id;

    final public function getId()
    {
        return $this->id;
    }

    private $url;

    final public function getUrl()
    {
        return $this->url;
    }

    private $title;

    final public function getTitle()
    {
        return $this->title;
    }

    private $image;

    final public function getImage()
    {
        return $this->image;
    }

    private $time;

    final public function getTime()
    {
        return $this->time;
    }

    private $votes;

    final public function getVotes()
    {
        return $this->votes;
    }

    private $comments;

    final public function getComments()
    {
        return $this->comments;
    }

    public function __construct($id, $url, $title, Image $image, $time, $votes, $comments)
    {
        $this->id = $id;
        $this->url = $url;
        $this->title = $title;
        $this->image = $image;
        $this->time = $time;
        $this->votes = $votes;
        $this->comments = $comments;
    }
}
