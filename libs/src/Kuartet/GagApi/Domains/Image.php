<?php

namespace Kuartet\GagApi\Domains;

final class Image
{
    private $thumb;

    final public function getThumb()
    {
        return $this->thumb;
    }

    private $small;

    final public function getSmall()
    {
        return $this->small;
    }

    private $big;

    final public function getBig()
    {
        return $this->big;
    }

    public function __construct($id, $image)
    {
        $dir = dirname($image);
        
        $pathInfo = pathinfo($image);
        extract($pathInfo);
        
        $filenameElements = explode('_', $filename);
        $suffix = '';
        if (count($filenameElements) == 3)
            $suffix = '_'.$filenameElements[2];

        $this->thumb = "{$dirname}/{$id}_220x145{$suffix}.{$extension}";
        $this->small = $image;
        $this->big = "{$dirname}/{$id}_700b{$suffix}.{$extension}";
    }
}
