<?php

namespace Kuartet\GagApi\Core\UrlFetcher;

interface UrlFetcher
{
    function fetch($url);
}
