<?php

namespace Kuartet\GagApi\Domains;

final class Section
{
    const HOT = 'hot';
    const TRENDING = 'trending';
}
