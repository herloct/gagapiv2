<?php

namespace Kuartet\GagApi\Core\UrlFetcher;

final class FileGetContentsUrlFetcher implements UrlFetcher
{
    final public function fetch($url)
    {
        return file_get_contents($url);
    }
}
