<?php

namespace Kuartet\GagApi\Repositories;

use Symfony\Component\DomCrawler\Crawler;
use Kuartet\GagApi\Domains\Section;
use Kuartet\GagApi\Repositories\GagsRepository;
use Doctrine\Common\Cache\Cache;

final class CachedGagsRepository implements GagsRepository
{
    private $repository;
    private $cache;
    private $timeout;

    public function __construct(GagsRepository $repository, Cache $cache, $timeout = 120)
    {
        $this->repository = $repository;
        $this->cache = $cache;
        $this->timeout = $timeout;
    }

    final public function findAll($section = Section::HOT, $page = 0)
    {
        $cacheId = "find_all_gags_{$section}_{$page}";
        if (! ($result = $this->cache->fetch($cacheId))) {
            $result = $this->repository->findAll($section, $page);

            $this->cache->save($cacheId, $result, $this->timeout);
        }

        return $result;
    }
}
