<?php

namespace Kuartet\GagApi\Repositories;

use Symfony\Component\DomCrawler\Crawler;
use Kuartet\GagApi\Domains\Attributes;
use Kuartet\GagApi\Domains\Section;
use Kuartet\GagApi\Domains\Image;
use Kuartet\GagApi\Domains\Gag;
use Kuartet\GagApi\Domains\Gags;
use Kuartet\GagApi\Core\UrlFetcher\UrlFetcher;

final class SiteGagsRepository implements GagsRepository
{

    private $urlFetcher;

    public function __construct(UrlFetcher $urlFetcher)
    {
        $this->urlFetcher = $urlFetcher;
    }

    private function getUrl($section = Section::HOT, $page = 0)
    {
        $pagePath = '';
        if (0 != $page)
            $pagePath = "/id/{$page}";

        return "http://9gag.com/{$section}{$pagePath}";
    }

    final public function findAll($section = Section::HOT, $page = 0)
    {
        $url = $this->getUrl($section, $page);
        $html = $this->urlFetcher->fetch($url);
        $crawler = new Crawler($html);

        $gags = array();
        $liDoms = $crawler->filter('ul#entry-list-ul > li');
        foreach ($liDoms as $liDom) {
            $liCrawler = new Crawler($liDom);
            $id = (float) $liCrawler->attr('gagid');

            $imgCrawler = $liCrawler->filter('div.content img[style="max-width:460px;"]')->first();
            if (count($imgCrawler) === 0)
                continue;

            $imgAlt = $imgCrawler->attr('alt');
            if ('NSFW' == $imgAlt || ! $imgAlt)
                continue;

            $imgSmallUrl = $imgCrawler->attr('src');
            $imgUrl = dirname($imgSmallUrl);

            $url = $liCrawler->attr('data-url');
            $title = $liCrawler->attr('data-text');
            $image = new Image($id, $imgSmallUrl);
            $votes = (float) $liCrawler->filter("span[votes]")->first()->text();
            $comments = (float) trim($liCrawler->filter("span.comment")->first()->text());
            $gags[] = new Gag($id, $url, $title, $image, '', $votes, $comments);
        }

        $next = (float) $crawler->filter('div#pagination > a#next_button')->attr('data-more');
        $attributes = new Attributes($next);

        return new Gags($attributes, $gags);
    }
}
