<?php

$include = realpath(__DIR__.'/../libs/vendor/autoload.php');
require $include;

use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\GetSetMethodNormalizer;
use Doctrine\Common\Cache\ApcCache;
use Kuartet\GagApi\Domains\Section;
use Kuartet\GagApi\Repositories\CachedGagsRepository;
use Kuartet\GagApi\Repositories\SiteGagsRepository;
use Kuartet\GagApi\Core\UrlFetcher\CurlUrlFetcher;

$page = isset($_GET['page']) ? (float) $_GET['page'] : 0;
$section = isset($_GET['section']) ? strtolower($_GET['section']) : Section::HOT;
if (! in_array($section, array(Section::HOT, Section::TRENDING)))
    $section = Section::HOT;

$encoders = array(new JsonEncoder());
$normalizers = array(new GetSetMethodNormalizer());

$serializer = new Serializer($normalizers, $encoders);

$repo = new CachedGagsRepository(
    new SiteGagsRepository(new CurlUrlFetcher()),
    new ApcCache(),
    300
);

$gags = $repo->findAll($section, $page);

if (! headers_sent()) {
    header('Content-Type: application/json');
    header('Cache-Control: public, max-age=60');
    header('Access-Control-Allow-Origin: *');
}
echo $serializer->serialize($gags, 'json');
