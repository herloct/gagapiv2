<?php

namespace Kuartet\GagApi\Domains;

final class Attributes
{
    private $next;

    final public function getNext()
    {
        return $this->next;
    }

    public function __construct($next)
    {
        $this->next = $next;
    }
}
