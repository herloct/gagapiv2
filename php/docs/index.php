<html>
<head>
    <title>Unofficial 9Gag Web API</title>
    <link href='http://fonts.googleapis.com/css?family=Droid+Sans:400,700' rel='stylesheet' type='text/css'/>
    <link href='css/screen.css' media='screen' rel='stylesheet' type='text/css'/>
    <script src='js/jquery-1.8.0.min.js' type='text/javascript'></script>
    <script src='js/jquery.slideto.min.js' type='text/javascript'></script>
    <script src='js/jquery.wiggle.min.js' type='text/javascript'></script>
    <script src='js/jquery.ba-bbq.min.js' type='text/javascript'></script>
    <script src='js/handlebars.runtime-1.0.0.beta.6.js' type='text/javascript'></script>
    <script src='js/underscore-min.js' type='text/javascript'></script>
    <script src='js/backbone-min.js' type='text/javascript'></script>
    <script src='js/swagger.js' type='text/javascript'></script>
    <script src='js/swagger-ui.min.js' type='text/javascript'></script>

    <style type="text/css">
        .swagger-ui-wrap {
            max-width: 960px;
            margin-left: auto;
            margin-right: auto;
        }

        .icon-btn {
            cursor: pointer;
        }

        #message-bar {
            min-height: 30px;
            text-align: center;
            padding-top: 10px;
        }

        .message-success {
            color: #89BF04;
        }

        .message-fail {
            color: #cc0000;
        }
    </style>

    <script type="text/javascript">
        $(function () {
            window.swaggerUi = new SwaggerUi({
                discoveryUrl:"./resources.json",
                apiKey:"",
                dom_id:"swagger-ui-container",
                supportHeaderParams: false,
                supportedSubmitMethods: ['get']
            });

            window.swaggerUi.load();
        });

    </script>
</head>

<body>
<div id='header'>
    <div class="swagger-ui-wrap">
        <a id="logo" href="http://gagapiv2-herloct.rhcloud.com/docs">Unofficial 9Gag Web API</a>
    </div>
</div>

<div id="message-bar" class="swagger-ui-wrap">
    &nbsp;
</div>

<div id="swagger-ui-container" class="swagger-ui-wrap">

</div>

</body>

</html>
