<?php

namespace Kuartet\GagApi\Repositories;

use Kuartet\GagApi\Domains\Section;

interface GagsRepository
{
    public function findAll($section = Section::HOT, $page = 0);
}
