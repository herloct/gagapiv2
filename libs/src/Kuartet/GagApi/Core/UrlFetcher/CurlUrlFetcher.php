<?php

namespace Kuartet\GagApi\Core\UrlFetcher;

final class CurlUrlFetcher implements UrlFetcher
{
    final public function fetch($url)
    {
        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_HEADER, FALSE);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);

        curl_setopt($ch, CURLOPT_ENCODING, 'gzip,deflate,sdch');

        $result = curl_exec($ch);

        curl_close($ch);

        return $result;
    }
}
